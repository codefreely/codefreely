Rails.application.routes.draw do
  devise_for :users
	resources :projects, shallow: true do
		resources :containers do
			member do
		    put 'start'
		    delete 'stop'
		  end
		end
		member do
	    put 'start_codebox'
	    delete 'stop_codebox'
	  end
	end
	root 'projects#index'
	get 'api/refresh/:api_key', :to => 'api#refresh', defaults: { format: 'json' }
end
