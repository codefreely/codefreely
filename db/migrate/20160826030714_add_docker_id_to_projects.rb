class AddDockerIdToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :docker_id, :string
  end
end
