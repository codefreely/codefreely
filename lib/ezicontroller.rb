class EziController
	def initialize (options)
		create = options[:create]
		if create
			@container =
			dockerc.create(
			  'Image' => options[:image],
			  'ExposedPorts' => formatExposedPorts(options[:ports]),
				'Cmd' => options[:command],
				'HostConfig' => {
					'Binds' => options[:binds],
					'PortBindings' => formatPortBindings(options[:ports])
				}
			)
		else
			@name = options[:name]
			@container = dockerc.get(@name)
		end
	end

	def raw
		return @container
	end

	def id
		return @container.json['Id']
	end

	def running
		return @container.json['State']['Running']
	end

	private
		def dockerc
			if !defined?(Docker)
				require 'docker'
				Docker.url = 'unix:///var/run/docker.sock'
			end
			return Docker::Container
		end

		def formatExposedPorts(ports)
			newPorts = {}
			ports.each do |int, ext|
				newPorts[int] = {}
			end
			return newPorts
		end

		def formatPortBindings(ports)
			newPorts = {}
			ports.each do |int, ext|
				newPorts[int] = [{ 'HostPort' => ext.to_s }]
			end
			return newPorts
		end
end
