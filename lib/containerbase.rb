require 'ezicontroller'

module ContainerBase
	def controller
		if docker_id != nil
			begin
				return EziController.new :name => docker_id
			rescue Docker::Error::NotFoundError
				return nil
			end
		else
			return nil
		end
	end

	def running
		c = controller
		if c != nil then
			return c.running
		end

		return false
	end
end
