require 'ezicontroller'
require 'containerbase'

class Project < ApplicationRecord
	has_many :containers, dependent: :restrict_with_exception
  belongs_to :user
	include ContainerBase

	def base_dir
		return Settings.filesystem.base
	end

	def mount_name
		return base_dir + '/' + user.id.to_s + '/' + id.to_s
	end

	def host_port
		return 8000 + id
	end
end
