require 'containerbase'

class Container < ApplicationRecord
  belongs_to :project
	include ContainerBase

	def mount_name
		return project.mount_name + '/' + id.to_s
	end

	def host_port
		return 9000 + id
	end
end
