class User < ApplicationRecord
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
				 :recoverable, :rememberable, :trackable, :validatable
	has_many :projects, dependent: :restrict_with_exception

	def name
		n = super
		if n == nil
			return email
		end
		return n
	end

	#Custom validation
	validate :presence_domain_email

	def presence_domain_email
		#if email domain not on valid list then return an error.

		valid = false

		Settings.valid_emails.each do |test_text|
			test = Regexp.new(test_text, Regexp::IGNORECASE)
			if test.match(email)
				valid = true
			end
		end

		# Generate an error if the domain is not valid
		unless valid
			errors.add(:email, 'does not match any approved condition.')
		end
	end
end
