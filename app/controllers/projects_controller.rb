class ProjectsController < ApplicationController
	before_action :authenticate_user!

	def initialize
		super
		@codebox_base = '/codebox/';
	end

	def new
		@project = Project.new
	end

	def index
		@user = current_user
		@projects = @user.projects
	end

	def show
		@project = Project.find(params[:id])
		@containers = @project.containers
		@user = @project.user
		@running = false

		if @project.docker_id != nil then
			begin
				container = EziController.new :name => @project.docker_id
				@running = container.running
			rescue
			end
		end
	end

	def create
		@user = current_user
		@project = @user.projects.create(project_params)

		mount_name = @project.mount_name
		host_port = @project.host_port

		if @project.save
			require 'ezicontroller'
			container = EziController.new :create => true,
					:binds => [mount_name + ':/opt/data'],
					:ports => {'8000/tcp' => host_port.to_s},
					:image => 'icecreammatt/codebox',
					:command => ["run", "/opt/data"]
			@project.docker_id = container.id
			@project.save

			redirect_to [@project]
		else
			render 'new'
		end
	end

	def destroy
		@project = Project.find(params[:id])
		@user = @project.user
		if @project.containers.length != 0
			redirect_to projects_path
			return
		end

		if @project.docker_id != nil then
			begin
				container = EziController.new :name => @project.docker_id
				container.raw.delete(:force => true)
			rescue Docker::Error::NotFoundError
				# why do we care? We were only going to delete it anyway.
			end
		end

		@project.destroy
		redirect_to projects_path
	end

	def start_codebox
		@project = Project.find(params[:id])
		controller = @project.controller

		if controller != nil then
			controller.raw.start
		end

		ApiController.send_updates_to_proxy
		redirect_to project_path(@project)
	end

	def stop_codebox
		@project = Project.find(params[:id])
		controller = @project.controller

		if controller != nil then
			controller.raw.stop
		end

		ApiController.send_updates_to_proxy
		redirect_to project_path(@project)
	end

	private
		def project_params
			params.require(:project).permit(:name, :description)
		end
end
