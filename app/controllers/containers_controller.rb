class ContainersController < ApplicationController
	before_action :authenticate_user!

	def new
		@project = Project.find(params[:project_id])
		@container = Container.new
	end

	def show
		@container = Container.find(params[:id])
		@project = @container.project
		@running = @container.running
	end

	def create
		@project = Project.find(params[:project_id])
		@container = @project.containers.create(container_params)

		if @container.save
			mount_name = @container.mount_name
			host_port = @container.host_port

			require 'ezicontroller'
			container = EziController.new :create => true,
																		:binds => [mount_name + ':/opt/data'],
																		:ports => {'80/tcp' => host_port.to_s},
																		:image => 'php7-mariadb-apache-opensuse'
			@container.docker_id = container.id
			@container.save

			# redirect_to project_containers_path(@project)
			redirect_to @container
		else
			render 'new'
		end
	end

	def destroy
		@container = Container.find(params[:id])
		@project = @container.project

		if @container.docker_id != nil then
			begin
				container = EziController.new :name => @container.docker_id
				container.raw.delete(:force => true)
			rescue Docker::Error::NotFoundError
				# why do we care? We were only going to delete it anyway.
			end
		end

		@container.destroy
		redirect_to project_containers_path(@project)
	end

	def start
		@container = Container.find(params[:id])
		controller = @container.controller

		if controller != nil then
			controller.raw.start
		end

		ApiController.send_updates_to_proxy
		redirect_to @container
	end

	def stop
		@container = Container.find(params[:id])
		controller = @container.controller

		if controller != nil then
			controller.raw.stop
		end

		ApiController.send_updates_to_proxy
		redirect_to @container
	end

	private
	def container_params
		params.require(:container).permit(:name, :description)
	end
end
