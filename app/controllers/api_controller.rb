class ApiController < ApplicationController
	def refresh
		respond_to do |format|
			if ApiController.is_key_valid(params[:api_key])
				msg = {:responce => 'ok'}
				ApiController.send_updates_to_proxy
			else
				msg = {:responce => 'badkey'}
			end
			format.json { render :json => msg }
		end
	end

	def self.api_key
		return Settings.proxy.api_key
	end

	def self.proxy
		return Settings.networking.proxy
	end

	def self.is_key_valid(key)
		return api_key == key
	end

	def self.send_updates_to_proxy
		require 'net/http'

		projects_srv = Settings.networking.docker.codeboxes
		containers_srv = Settings.networking.docker.containers

		projects_data = {}
		projects = Project.all
		projects.each do |proj|
			controller = proj.controller
			if controller != nil && controller.running
				projects_data[proj.id] = {:address => "http://" + projects_srv + ":" + proj.host_port.to_s}
			end
		end

		containers_data = {}
		containers = Container.all
		containers.each do |cont|
			controller = cont.controller
			if controller != nil && controller.running
				containers_data[cont.id] = {:address => "http://" + containers_srv + ":" + cont.host_port.to_s}
			end
		end

		data = {
				:api_key => api_key,
				:projects => projects_data,
				:containers => containers_data
		}.to_json

		uri = URI.parse(proxy + '/proxy-api/setproject/' + URI.escape(data))
		resp = Net::HTTP.get(uri)
	end

	private_class_method :api_key
end
